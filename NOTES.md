**Code**
https://gitlab.com/olmesm/buttered-react

**Sample site**
https://olmesm.gitlab.io/buttered-react

**Things to note:**

- You cant list the pages in ButterCMS. I’ve accomplished this with an “Exposed Pages” collection. This acts as the entry point for the site.
- Components are represented by collections. This allows us to define the data type and re-use them across the site.
- I’ve not used page-types and dont think we should rely on them as far as development and the API is concerned. Reason being is it restricts the components we can then select to be placed on the page. However there is no reason marketing cant use it.
- there’s no graphql API like I originally thought. If we wanted one we’d have to make it (this could help with caching and speed but I dont think this should be an MVP) Use this as a starting point <https://github.com/ButterCMS/gatsby-source-buttercms/blob/master/src/gatsby-node.js>
- If we were inclined to do gatsby (which I really dont think is a bad shout) there’s loads of information here <https://www.gatsbyjs.org/docs/sourcing-from-buttercms/>.
  It’s also got data on how to trigger a build from a webhook. I feel like this reduces the complexity in comparison to prerender, but it’s your call.
- there's an axios hook for caching increasing the performance of network requests <https://www.npmjs.com/package/buttercms#axios-hook>
