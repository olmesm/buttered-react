import Butter from "buttercms";
const { REACT_APP_BUTTER_CMS_API_TOKEN } = process.env;
const EXTENDED_TIMEOUT = 15000;

export const butter = Butter(
  REACT_APP_BUTTER_CMS_API_TOKEN!,
  false,
  EXTENDED_TIMEOUT
);
