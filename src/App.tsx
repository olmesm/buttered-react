import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { butter } from "./services/api-client";
import { componentList } from "./components";

interface ExposedPage {
  buttercms_api_slug: string;
  optional_display_name_override?: string;
  optional_url_override?: string;
  has_children?: ExposedPage[];
  is_in_main_navigation?: true;
}

interface Page {
  slug: string;
  fields: {
    [key: string]: any;
  };
}

const { REACT_APP_PUBLIC_URL } = process.env;
const ENTRY_POINT = "exposed_pages";

const getExposedPageList = async () => {
  return await butter.content.retrieve([ENTRY_POINT]);
};

const getPage = async (apiSlug: string) => {
  return await butter.page.retrieve("*", apiSlug);
};

export const App: React.FC = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [pageList, setPageList] = useState<ExposedPage[]>([]);
  const [pages, setPages] = useState<Page[]>([]);

  useEffect(() => {
    (async () => {
      const rawPageList = await getExposedPageList();

      setPageList(rawPageList.data.data[ENTRY_POINT]);
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const _pages = await Promise.all(
        pageList.map(async page => {
          try {
            const rawPage = (await getPage(page.buttercms_api_slug)) as any;

            return rawPage.data.data;
          } catch (e) {
            return undefined;
          }
        })
      );

      setPages(_pages);
      setIsLoading(false);
    })();
  }, [pageList]);

  if (isLoading) {
    return <h1>loading...</h1>;
  }

  return (
    <Router basename={REACT_APP_PUBLIC_URL || ""}>
      <div>
        <nav>
          <ul>
            {pageList
              .filter(page => page.is_in_main_navigation)
              .map(page => {
                const foundPage = pages.find(
                  _page => _page.slug === page.buttercms_api_slug
                );

                if (!foundPage) {
                  return (
                    <React.Fragment
                      key={`navigation-${page.buttercms_api_slug}`}
                    ></React.Fragment>
                  );
                }

                return (
                  <li key={`navigation-${page.buttercms_api_slug}`}>
                    <Link
                      to={page.optional_url_override || page.buttercms_api_slug}
                    >
                      {page.optional_display_name_override ||
                        foundPage.fields.headline}
                    </Link>
                  </li>
                );
              })}
          </ul>
        </nav>

        <Switch>
          {pageList.map(page => {
            const foundPage = pages.find(
              _page => _page.slug === page.buttercms_api_slug
            );

            if (!foundPage) {
              return (
                <React.Fragment
                  key={`route-${page.buttercms_api_slug}`}
                ></React.Fragment>
              );
            }

            return (
              <Route
                key={`route-${page.buttercms_api_slug}`}
                path={`/${page.optional_url_override ||
                  page.buttercms_api_slug}`}
              >
                <PageTemplate foundPage={foundPage} />
              </Route>
            );
          })}
        </Switch>
      </div>
    </Router>
  );
};

function PageTemplate({ foundPage }: { foundPage: Page }) {
  let pageComponents = [];
  let pageMeta = [];
  for (const property in foundPage.fields) {
    const MatchedComponent = componentList.find(
      component => component.type === foundPage.fields[property].type
    );

    if (MatchedComponent) {
      pageComponents.push(
        <MatchedComponent.component
          key={`${foundPage.slug}--${MatchedComponent.type}-${property}`}
          {...foundPage.fields[property]}
        />
      );
    }

    pageMeta.push({ [property]: foundPage.fields[property] });
  }

  return <>{pageComponents}</>;
}
