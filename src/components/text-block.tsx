import React from "react";

export const TEXT_BLOCK = "text_block";

interface TextBlock {
  type: typeof TEXT_BLOCK;
  text: string;
}

export const TextBlock: React.FC<{ text: string }> = ({ text }) => (
  <div data-component-type={TEXT_BLOCK}>{text}</div>
);
