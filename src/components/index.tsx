import { TEXT_BLOCK, TextBlock } from "./text-block";

export const PAGE_LINK = "page-link";

export const componentList = [{ type: TEXT_BLOCK, component: TextBlock }];
