# Buttered React

Sample site using React and Butter CMS. See my [notes](./NOTES.md) for more

## Uses

- [ButterCMS]
- [React]
- [Typescript]
- [GraphQL]

## Requires

Please see .env.sample for required env settings and secrets.

- [nvm] (or node if you're a savage)
- [yarn]
- [ButterCMS] account

## Development

```sh
# Set node
nvm use

# Install yarn if not in this node version
npm install -g yarn

# Install Dependencies
yarn

# Start the development server
yarn start

# Build project
yarn build
```

# Deploying

<!-- Markdown References -->

[buttercms]: https://buttercms.com/
[graphql]: https://graphql.org/
[nvm]: https://github.com/nvm-sh/nvm
[react]: https://www.google.com/search?q=react&oq=react&aqs=chrome.0.0l3j69i61j69i60l2j69i65l2.919j0j4&sourceid=chrome&ie=UTF-8
[typescript]: https://www.typescriptlang.org/
[yarn]: https://yarnpkg.com/lang/en/
